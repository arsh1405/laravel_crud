<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('main', 'Form@index');

Route::post('save_category', 'Form@save_category');

Route::post('save_product', 'Form@save_product');

Route::post('update', 'Form@update');

Route::get('getAjax', 'Form@getAjax');
