<link href="{{ asset('css/form.css') }}" rel="stylesheet">
<div class="category_outer_area">
<div class="category_outer">
	<form id="category_form" method="post" action="{{url('/save_category')}}">
		{{csrf_field()}}
		Category:<input type="text" name="Category" id="category_type">
		<input type="submit" value="SUBMIT" id="category_type_submit">
	</form>
</div>
<div class="product_outer">
	<form id="product_form" method="post" action="{{url('/save_product')}}">
		{{csrf_field()}}
		<p>
			Category:<select name="category_id">
			 
			@foreach ($users as $user) 
				
				<option value="{{$user->id}}">{{$user->category_name}}</option>
			@endforeach	       
			</select>
		</p>
			
		<p>Product Type:<input type="text" name="product_type" id="product_type"></p>
 		<p>Product Description:<input type="text" name="product_des" id="product_des"></p>
        <p>Product Value<input type="text" name="product_value" id="product_value"></p>
		<p><input type="submit" value="SUBMIT" id="category_type_submit"></p>
	</form>
</div>

<div class="category_name">
  <span>Category</span>
  @foreach ($users as $user)  	
  <li>
	  <a href="" class="category_name_data" data-id="{{$user->id}}">{{$user->category_name}}</a> 
  </li>	 
  @endforeach	   
</div>
<span id="product_content"></span>
</div>


<script src="{{ asset('js/jquery-3.3.1.js') }}"></script>
<script>
    $(document).ready(function(e){
     
        $(document).on('click', '.category_name_data', function(e){
		    e.preventDefault();
		    
		    var dataid = $(this).attr('data-id');
		    alert(dataid);
		    
		    $.ajax({
			    type: "GET",
			    url: "getAjax",
			    data: { "dataid": dataid },
			    success: function (data) {
			            console.log(data);
			            $('#product_content').html(data);   
			    }
			});
		    
		   
		
		});
    });
</script>



