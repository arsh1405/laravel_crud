<div class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Product details</h5>
      </div>
      <div class="modal-body">
        <form id="product_form" method="post" action="{{url('/update')}}">
			{{csrf_field()}}
            @foreach ($users as $user) 
             <div>
			   <input type="text" name="category_id"	value="{{$user->category_id}}">
             <p>
				Product Name:<input type="text" name="product_type" value="{{$user->product_name}}">
		     </p>
		     <p>
				Product Price:<input type="text" name="product_value" value="{{$user->product_price}}">
		     </p>
		     <p>
				Product Description:<input type="text" name="product_des" value="{{$user->product_description}}">
		     </p>
		     </div>
		    @endforeach	  
		    <button type="submit" class="btn btn-primary">Update</button>
		</form>    
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

