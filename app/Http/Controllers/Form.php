<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\formcategory;
use App\formproduct;
use Session;


class Form extends Controller
{
    public function index()
    {
	    $users = DB::table('form')->get();
        return view('form',['users' => $users]);
    }
    
    public function save_category(Request $request)
    {
        $this->validate(request(),[
         
        ]);         
       
        $user= new formcategory();
        $user->category_name= $request['Category'];
        
        
        if($user->save()) {
		   $users = DB::table('form')->get();
        }
        
        return view('form',['users' => $users]);
        
    }
    
    public function save_product(Request $request)
    {
		$this->validate(request(),[
         
        ]);         
        
        $user= new formproduct();
        $user->category_id= $request['category_id'];
        $user->product_name= $request['product_type'];
        $user->product_price= $request['product_value'];
        $user->product_description= $request['product_des'];
        $user->save();
        
        $test = DB::table('form')->get();
                
        return view('form',['users' => $test] , );
	}
	
	public function getAjax(Request $request)
	{ 
		$data= $request->dataid;
		$test = new formproduct();
		$result = DB::table('product')->where('category_id', $data )->get();
		echo view('view_product',['users' => $result]);die;
	}
    
    public function update(Request $request)
    {
		$val = DB::table('product')
               ->where('category_id', $request['category_id'])
               ->update(['product_name' => $request['product_type'],'product_price' => $request['product_value'],'product_description' => $request['product_des']]);
               
        Session::flash('message', 'This is a message!');           
		$users = DB::table('form')->get();
        return view('form',['users' => $users]);
	}
     
     	
}
