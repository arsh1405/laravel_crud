<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class formproduct extends Model
{
     protected $fillable = [
        'category_id',
        'product_name',
        'product_price',
        'product_description',
        // add all other fields
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $table = 'product';
}
