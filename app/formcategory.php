<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class formcategory extends Model
{
    protected $fillable = [
        'category_name',
        // add all other fields
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $table = 'form';
    
    
    
    public function get_data(){
		
		$posts = formcategory::all();

		return $posts; 
	}
    
    
}
